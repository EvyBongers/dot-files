#!/usr/bin/env bash
set -eu

SCRIPTS_DIR="$(dirname "${BASH_SOURCE[0]}")"
DOT_FILES_DIR="$(dirname "${SCRIPTS_DIR}")"

NUM_FAILED=0

for f in "${DOT_FILES_DIR}"/*
do
    # scripts dir should not be symlinked
    [[ "$(realpath "${SCRIPTS_DIR}")" == "$(realpath "${f}")" ]] && continue

    unset link
    test -e "${HOME}/.${f##*/}" && link="$(readlink "${HOME}/.${f##*/}")"
    real="$(realpath "${f}")"

    if [[ "${link:-}" != "${real}" ]]
    then
        NUM_FAILED=$((NUM_FAILED+1))
        echo "File ${f} not symlinked"
        echo "Link: ${link:-}"
        echo "Real: ${real}"
        echo ""
    fi
done

if [[ ${NUM_FAILED} -eq 0 ]];
then
    echo "All links set up!"
fi
